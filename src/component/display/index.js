import { useSelector} from 'react-redux';

const Display = () => {
    const number = useSelector((state) => state.number)
    return(
    <div>
        { number }
    </div>)
}

export default Display;