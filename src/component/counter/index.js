import { useDispatch } from 'react-redux';
import { useState } from 'react';
import { numberPlus, numberMinus, numberMultiply, numberDivide } from '../../store/modules/result/actions';


const Counter = () => {
    const dispatch = useDispatch();
    const [ num1, setNum1 ] = useState(0)
    const [ num2, setNum2 ] = useState(0)

    const changeNum1 = (event) => {
        setNum1( Number(event.target.value) )
    }

    const changeNum2 = (event) => {
        setNum2( Number(event.target.value) )
    }

    return(
        <div>
            <div>
                <input type='Number' style={{width:'50px'}} onChange={changeNum1} value={num1}/>
                <input type='Number' style={{width:'50px'}} onChange={changeNum2} value={num2}/>
            </div>
            <div>
                <button onClick={() => dispatch(numberMinus(num1, num2))}> - </button>
                <button onClick={() => dispatch(numberPlus(num1, num2))}> + </button>
            </div>
            <div>
                <button onClick={() => dispatch(numberMultiply(num1, num2))}> * </button>
                <button onClick={() => dispatch(numberDivide(num1, num2))}> / </button>
            </div>
        </div>
    )}

export default Counter;