import { createStore, combineReducers } from 'redux';

import numberReducer from './modules/result/reducer';

const reducers = combineReducers({number: numberReducer});

const store = createStore(reducers);

export default store;