export const numberPlus = (num1, num2) => ({
    type: 'NUMBER_PLUS',
    num1, 
    num2,
})

export const numberMinus = (num1, num2) => ({
    type: 'NUMBER_MINUS',
    num1, 
    num2,
})

export const numberMultiply = (num1, num2) => ({
    type: 'NUMBER_MULTIPLY',
    num1, 
    num2,
})

export const numberDivide = (num1, num2) => ({
    type: 'NUMBER_DIVIDE',
    num1, 
    num2,
})