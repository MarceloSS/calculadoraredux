const numberReducer = (state = 0, action) => {
    switch(action.type){
        case "NUMBER_PLUS":
            
            state = action.num1 + action.num2

            return state;

        case "NUMBER_MINUS":

            return action.num1 - action.num2;
            
        case "NUMBER_MULTIPLY":

            return action.num1 * action.num2;
    
        case "NUMBER_DIVIDE":

            return action.num1 / action.num2;
    
        default:

            return state;

    }
}

export default numberReducer;