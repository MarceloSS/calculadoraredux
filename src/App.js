import './App.css';

import Counter from './component/counter'
import Display from './component/display'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Display/>
        <Counter/>
      </header>
    </div>
  );
}

export default App;
